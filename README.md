# php-exercises

## 1/ HELLO WORLD !

### Instructions
The classical introductory exercise. Just say "Hello, World!".

"Hello, World!" is the traditional first program for beginning programming in a new language or environment.

The objectives are simple:

Write a function that returns the string "Hello, World!".
Run the test suite and make sure that it succeeds.
Submit your solution and check it at the website.
If everything goes well, you will be ready to fetch your first real exercise.

```php
<?php

function helloWorld()
{
    return "Good Bye, World!";
}
```

## 2/ Reverse String

### Instructions
Reverse a string

For example: input: "cool" output: "looc"

```php
<?php

function reverseString(string $text): string
{
    return '';
}
```

## 3/ Robot Name

### Instructions
Manage robot factory settings.

When a robot comes off the factory floor, it has no name.

The first time you turn on a robot, a random name is generated in the format of two uppercase letters followed by three digits, such as RX837 or BC811.

Every once in a while we need to reset a robot to its factory settings, which means that its name gets wiped. The next time you ask, that robot will respond with a new random name.

The names must be random: they should not follow a predictable sequence. Using random names means a risk of collisions. Your solution must ensure that every existing robot has a unique name.

```php
<?php

class Robot
{
    public function getName(): string
    {
        throw new \BadMethodCallException("Implement the getName method");
    }

    public function reset(): void
    {
        throw new \BadMethodCallException("Implement the reset method");
    }
}
```

## 4/ Mask credit card

### Instructions
Create a function `maskify` to mask digits of a credit card number with `#`.

**Requirements:**

- Do not mask the first digit and the last four digits
- Do not mask non-digit chars
- Do not mask if the input is less than 6
- Return '' when input is empty

**Examples:**

- `1234-5678-9012` converts to `1###-####-9012`
- `123456789012` converts to `1#######9012`

```php
function maskify(string $cc): string
{
    throw new \BadFunctionCallException("Implement the maskify function");
}
```
