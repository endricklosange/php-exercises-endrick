<?php

class Robot
{ 
    
    public function __construct()
    {
        if(!isset($allName)){
            global $allName;
            $allName =[];
        }     
    }
    
    public function getName()
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $var_size = strlen($chars);
        $name = '';
        for ($i = 0; $i < 2; $i++) {
            $random_str = $chars[rand(0, $var_size)];
            $name .= $random_str;
        }
        $name .=  rand(100,999);
        if(!in_array($name, $GLOBALS["allName"])){
            $GLOBALS["allName"][] = $name;
        return $name; 
        }
        else{
            $this->reset();
        }   
    }
    public function reset(){
       return false;
    }
}
