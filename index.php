<?php

require_once 'exercise1.php';
require_once 'exercise2.php';
require_once 'exercise4.php';
require_once 'Robot.php';
require_once 'Clock.php';

try {
    echo 'Current PHP version: ' . phpversion();
    echo '<br />';

    $host = 'db';
    $dbname = 'database';
    $user = 'user';
    $pass = 'pass';
    $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";
    $conn = new PDO($dsn, $user, $pass);
    $robt1 = new Robot;
    $robt2 = new Robot;
    $clock1 = new Clock;
    echo '<pre>';

    echo '<pre />';

    echo '<br />';
    //ex1
    echo helloWorld();
    echo '<br />';
    //ex2
    echo reverseString('cool');
    echo '<br />';
    //ex3
    echo $robt1->getName();
    echo '<br />';
    echo $robt2->getName();
    echo '<br />';
    echo maskify('2345678901test2166486486');
    echo '<br />';

    var_dump($clock1->setHoursPlus(68));

} catch (\Throwable $t) {
    echo 'Error: ' . $t->getMessage();
    echo '<br />';
}
